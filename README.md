# YASFBM - Yet Another SDK for Bouffalo MCU

The goal of this (experimental - work in progress) project is to build a lightweight SDK for Bouffalo MCU (BL60x/BL70x) that is easy to integrate into existing CMake projects.

## How to build

```
$ mkdir build
$ cd build
$ cmake -DRISCV_UNKNOWN_ELF_TOOLCHAIN_BIN_PATH=/Path to your RISC-V toolchain/bin/ ..
$ make
```

This will generate the static library `libyasfbm.a`.

## How to integrate in a CMake project

Here is a basic CMakeFile.txt for an application composed of 1 source file (`main.cpp`) : 

```cmake
cmake_minimum_required(VERSION 3.23)
project(yasfbm_example)
add_subdirectory(sdk/yasfbm)

include(sdk/yasfbm/cmake/riscv64-unknown-elf-gcc.cmake)

set(CHIP bl602)
SET(CPU_ARCH "RISCV")
SET(MCPU "e24")
SET(MARCH "rv32imafc")
SET(MABI "ilp32f")
SET(EMULATION_MODE "elf32-littleriscv")

set(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/sdk/yasfbm/bl602_flash.ld)
set(MAP_FILE ${CMAKE_BINARY_DIR}/yasfbm_example.map)
set(HEX_FILE ${CMAKE_BINARY_DIR}/yasfbm_example.hex)
set(BIN_FILE ${CMAKE_BINARY_DIR}/yasfbm_example.bin)
set(ASM_FILE ${CMAKE_BINARY_DIR}/yasfbm_example.asm)

add_executable(yasfbm_example
    main.cpp)

target_link_libraries(yasfbm_example
    yasfbm)

target_compile_options(yasfbm_example PRIVATE
    -O2 -g3
    -fno-jump-tables -fshort-enums -fno-common -fms-extensions -ffunction-sections -fdata-sections -fstrict-volatile-bitfields -ffast-math
    -Wall -Wshift-negative-value -Wchar-subscripts -Wformat -Wuninitialized -Winit-self -Wignored-qualifiers -Wunused -Wundef
    -msmall-data-limit=4
    -Wtype-limits
    -march=${MARCH} -mabi=${MABI}
    )

target_compile_options(yasfbm_example PRIVATE
    $<$<COMPILE_LANGUAGE:C>:-std=c99>
    $<$<COMPILE_LANGUAGE:CXX>:-std=c++17>
    $<$<COMPILE_LANGUAGE:CXX>:-nostdlib>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>
    )

target_link_options(yasfbm_example PRIVATE
    -Wl,--cref -Wl,--gc-sections -nostartfiles -g3
    -fms-extensions -ffunction-sections -fdata-sections
    -Wall -Wchar-subscripts -std=c99
    --specs=nano.specs
    -march=${MARCH} -mabi=${MABI}
    -Wl,-m,elf32lriscv
    )


set_target_properties(yasfbm_example PROPERTIES LINK_FLAGS "-T${LINKER_SCRIPT} -Wl,-Map=${MAP_FILE}")
set_target_properties(yasfbm_example PROPERTIES OUTPUT_NAME "yasfbm_example.elf")
set_target_properties(yasfbm_example PROPERTIES LINK_DEPENDS ${LINKER_SCRIPT})

add_custom_command(TARGET yasfbm_example POST_BUILD
    COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:yasfbm_example> ${BIN_FILE}
    COMMAND ${CMAKE_OBJDUMP} -d -S $<TARGET_FILE:yasfbm_example> >${ASM_FILE}
    COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:yasfbm_example> ${HEX_FILE}
    COMMAND ${SIZE} $<TARGET_FILE:yasfbm_example>
    )
```

Basically, it includes (`add_subdirectory()`) `yasfbm`. It also uses the toolchain definition file and the linker script from `yasfbm`. 

Then, it defines an executable (`add_executable()`) that links with `yasfbm`. It also defines the compilation and link options (the same than `yasfbm`).

Finally, it defines a few custom commands to convert the output `.elf` file into `.hex` and `.bin` files.

See [this example application](https://codeberg.org/JF002/yasfbm_example).


cmake_minimum_required(VERSION 3.22)
project(yasfbm C CXX ASM)

SET(YASFBM_TARGET_NAME yasfbm)

set(CHIP bl602)
SET(CPU_ARCH "RISCV")
SET(MCPU "e24")
SET(MARCH "rv32imafc")
SET(MABI "ilp32f")
SET(EMULATION_MODE "elf32-littleriscv")

include(${CMAKE_CURRENT_LIST_DIR}/cmake/riscv64-unknown-elf-gcc.cmake)

add_library(${YASFBM_TARGET_NAME} STATIC
    drivers/${CHIP}/startup/entry.S
    drivers/${CHIP}/startup/start_load.c
    drivers/${CHIP}/startup/system_${CHIP}.c
    drivers/${CHIP}/std_drv/src/${CHIP}_hbn.c
    drivers/${CHIP}/std_drv/src/${CHIP}_pds.c
    drivers/${CHIP}/std_drv/src/${CHIP}_ef_ctrl.c
    drivers/${CHIP}/std_drv/src/${CHIP}_common.c
    drivers/${CHIP}/std_drv/src/${CHIP}_glb.c
    )

target_compile_options(${YASFBM_TARGET_NAME} PRIVATE
    -O2 -g3
    -fno-jump-tables -fshort-enums -fno-common -fms-extensions -ffunction-sections -fdata-sections -fstrict-volatile-bitfields -ffast-math
    -Wall -Wshift-negative-value -Wchar-subscripts -Wformat -Wuninitialized -Winit-self -Wignored-qualifiers -Wunused -Wundef
    -msmall-data-limit=4
    -Wtype-limits
    -march=${MARCH} -mabi=${MABI}
    )

target_compile_options(${YASFBM_TARGET_NAME} PRIVATE
    $<$<COMPILE_LANGUAGE:C>:-std=c99>
    $<$<COMPILE_LANGUAGE:CXX>:-std=c++17>
    $<$<COMPILE_LANGUAGE:CXX>:-nostdlib>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>
    )

target_link_options(${YASFBM_TARGET_NAME} PRIVATE
    -Wl,--cref -Wl,--gc-sections -nostartfiles -g3
    -fms-extensions -ffunction-sections -fdata-sections
    -Wall -Wchar-subscripts -std=c99
    --specs=nano.specs
    -march=${MARCH} -mabi=${MABI}
    -Wl,-m,elf32lriscv)

target_compile_definitions(${YASFBM_TARGET_NAME} PUBLIC ARCH_RISCV)

target_include_directories(${YASFBM_TARGET_NAME} PUBLIC
    drivers/${CHIP}
    drivers/${CHIP}/regs
    drivers/${CHIP}/startup
    drivers/${CHIP}/std_drv/inc
    common/
    common/soft_crc
    )
